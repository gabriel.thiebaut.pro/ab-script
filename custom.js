// Initiatlisation du thème :
    let urlcourante = document.location.href;
    let url = new URL(urlcourante);
    let search_param = new URLSearchParams(url.search);

    if (search_param.has('theme')){

        let themename=search_param.get('theme');

        if(themename=="b"){
            initThemeB();
            console.log("Theme B");
        }
        if(themename=="a"){
            console.log("Theme A");
        }
        else{
            console.log("Theme A");
        }
    }

// Ajout des boutons de changement de thème :
    let logodusite = document.querySelector('.header-logo');
    logodusite.insertAdjacentHTML('afterend', "<a id='changement' href='http://" + window.location.host + "/?theme=b'>Theme B</a>");
    logodusite.insertAdjacentHTML('afterend', "<a id='changement' href='http://" + window.location.host + "/?theme=a'>Theme A</a>");

// Fonction d'ininiation du thème B :
    function initThemeB(){
        // On récupère le logo et on remplace la source de l'image pour mettre le logo bleu :
            document.querySelector('.header-logo img').src="img/logo-default.png";

        // On récupere la div col-lg-3 (Aside) et on la cache :
            document.querySelector('div.main > div.container > div.row > div.col-lg-3').style.display="none";

        // On récupere le micro formulaire "Add to cart" pour l'afficher en colonne :
            let cart = document.querySelector('.cart');
            cart.setAttribute("style" ,"display : flex; flex-direction: column; justify-content: space-around; align-items: center;")

        // Retirer les marges sur le selecteur  de quantité :
            document.querySelector('.cart > .quantity').style.margin="32px auto";

        // Changer le style du  bouton "Add to cart" :
            let cartbutton = document.querySelector('.cart > button');
            cartbutton.setAttribute("style", "height: 64px; width: 180px; font-size: 16px;");

        //Centrer la collone contenant la fiche du produit et l'élargir :
            document.querySelector('div.main > div.container > div.row > div.col-lg-9').className="col-lg-12";

        // Inverser Categories et le bouton "Add to cart" :
            let parent = document.querySelector('div.main > div.container > div.row > div.col-lg-12 > div.row > .col-lg-6:nth-child(2) div.summary');
            let form = document.querySelector('form.cart');
            let cat = document.querySelector('div.product-meta');
            parent.insertBefore(cat, form);
            form.style.marginTop="50px";

        //Changer les images du carousel :
            let carrouselimg = document.querySelectorAll('div.owl-carousel div img');
            carrouselimg[0].src='img/products/sac.jpg';
            carrouselimg[1].src='img/products/sac.jpg';
            carrouselimg[2].src='img/products/sac.jpg';

        //Changer les images des produits reliés :
            let masson =  document.querySelectorAll('.masonry-loader > .row > .product > span > a:nth-child(2) > span > img');
            masson[0].src='img/products/photo.jpg';
            masson[1].src='img/products/golf.jpeg';
            masson[2].src='img/products/workout.jpg';
            masson[3].src='img/products/sacluxe.jpg';

        // Modifier le micro formulaire d'inscription à la Newsletter :
            document.querySelector('form#newsletterForm div.input-group span.input-group-append button.btn').style.borderRadius='0';
    };